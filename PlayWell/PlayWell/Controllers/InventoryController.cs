﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlayWell.Data;
using PlayWell.Helper;
using PlayWell.Models;
using PlayWell.Models.InventoryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PlayWell.Controllers
{
    [Authorize]
    public class InventoryController : Controller
    {
        private readonly ApplicationDbContext context;

        private static InventoryViewModel storedViewModel;

        private static List<LegoItem> favorites;

        private static bool filterChanged;

        private static bool startingOver;

        private IHostingEnvironment environment;

        private PricingHelper pricingHelper;

        public InventoryController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
            this.pricingHelper = new PricingHelper(context);
            if (storedViewModel == null)
            {
                storedViewModel = new InventoryViewModel();
            }

            if (storedViewModel.ChosenList == null)
            {
                // Set the list to default "none".
                storedViewModel.ChosenList = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).FirstOrDefault(l => l.Id == 7);
            }
        }

        [HttpGet]
        public string AddToList(string itemId)
        {
            LegoList list = null;

            string result = "item added to list";

            try
            {
                int listBridgeId = int.Parse(itemId);

                ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);

                if (listBridgeId != 0)
                {
                    // If the user doesn't have any lists, create one
                    if (this.context.LegoLists.Any(l => l.ApplicationUserId == currentUser.Id) == false)
                    {
                        this.context.LegoLists.Add(new LegoList
                        {
                            ApplicationUserId = currentUser.Id,
                            Name = "Favorites"
                        });

                        this.context.SaveChanges();
                    }

                    // If the stored chosen list has not been set
                    if (storedViewModel.ChosenList != null)
                    {
                        // Set the list to the chosen list
                        list = storedViewModel.ChosenList;
                    }

                    if (list != null)
                    {
                        list.LastModified = DateTime.Now;

                        LegoList listToSave = this.context.LegoLists.FirstOrDefault(l => l.Id == list.Id);

                        if (listToSave != null)
                        {
                            listToSave.LastModified = list.LastModified;
                        }

                        ListBridge listBridge = this.context.ListBridges.FirstOrDefault(l => l.LegoItemId == listBridgeId);

                        if (listBridge == null)
                        {
                            // Add a list bridge for this item
                            this.context.ListBridges.Add(new ListBridge
                            {
                                LegoListId = list.Id,
                                LegoItemId = listBridgeId
                            });
                        }
                        else
                        {
                            this.context.ListBridges.Remove(listBridge);
                        }

                        this.context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }

        public IActionResult Favorites()
        {
            InventoryViewModel viewModel = new InventoryViewModel();
            viewModel.View = "Set pricing";
            viewModel.AmountToDisplay = 10;
            viewModel.Type = 2;
            startingOver = true;
            this.LoadFavorites();
            this.IncrementPaging(viewModel);
            this.SetFavorites(viewModel);
            this.LoadLookups(viewModel);
            this.SetLookups(viewModel);
            this.ResetCount(viewModel);

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Favorites(InventoryViewModel viewModel)
        {
            this.StoreViewModelData(viewModel);
            this.IncrementPaging(viewModel);
            this.SetFavorites(viewModel);
            this.SetLookups(viewModel);
            this.ResetCount(viewModel);

            return View(viewModel);
        }

        public IActionResult DataImport()
        {
            Importer importer = new Importer(context, environment);
            importer.SetBridgeDataImport();
            return View();
        }

        public IActionResult Index()
        {
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();

            return View();
        }

        /// <summary>
        /// Performs search on full item list, calculated total results and pages.
        /// </summary>
        /// <returns>A value indicating whether or not the page number is equal to the total pages.</returns>
        [HttpGet]
        public object InitiateTotals()
        {
            object result = null;

            if (filterChanged && !startingOver)
            {
                try
                {
                    this.SetTotals();
                }
                catch (Exception e)
                {
                    result = new { status = "failure", IsFinalPage = "false", message = e.ToString() };
                }
            }

            if (storedViewModel.PageNumber == storedViewModel.TotalPages)
            {
                result = new { status = "success", IsFinalPage = "true" };
            }
            else
            {
                result = new { status = "success", IsFinalPage = "false" };
            }

            return result;
        }

        public IActionResult ItemList(int listId)
        {
            InventoryViewModel viewModel = new InventoryViewModel();
			viewModel.LegoItems = new List<LegoItem>();
            viewModel.View = "Set pricing";
            viewModel.AmountToDisplay = 10;
            viewModel.Type = 2;
            storedViewModel.Filter = string.Empty;
            this.StoreViewModelData(viewModel);
            startingOver = false;
            storedViewModel.View = "Favorites";
            this.LoadChosenList(listId.ToString());
            this.SetChosenList(viewModel);
            this.IncrementPaging(viewModel);
            this.LoadLookups(viewModel);
            this.SetLookups(viewModel);
			this.LoadFavorites();
            this.SetFavorites(viewModel);
            this.ResetCount(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ItemList(InventoryViewModel viewModel)
        {
            this.StoreViewModelData(viewModel);
            this.SetChosenList(viewModel);
            this.IncrementPaging(viewModel);
			this.SetLookups(viewModel);
            this.SetFavorites(viewModel);
            this.ResetCount(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        [HttpGet]
        public string GetTotalPages()
        {
            return storedViewModel.TotalPages.ToString();
        }

        [HttpGet]
        public string GetTotalResults()
        {
            return storedViewModel.TotalResults.ToString();
        }

        public IActionResult Inventory()
        {
            InventoryViewModel viewModel = new InventoryViewModel();
            viewModel.LegoItems = new List<LegoItem>();
            viewModel.View = "Location";
            viewModel.AmountToDisplay = 10;
            viewModel.Type = 2;
            storedViewModel.Filter = string.Empty;
            startingOver = true;
            this.LoadFavorites();
            this.LoadLookups(viewModel);
            this.SetLookups(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Inventory(InventoryViewModel viewModel)
        {
            this.StoreViewModelData(viewModel);
            this.IncrementPaging(viewModel);
            this.SetChosenList(viewModel);
            this.SetLookups(viewModel);
            this.SetLegoItems(viewModel);
            this.PlugInDelegates(viewModel);
            this.ResetCount(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        public IActionResult Invoice()
        {
            return View();
        }

        public IActionResult MasterList(int? page)
        {
            InventoryViewModel viewModel = new InventoryViewModel();
            viewModel.LegoItems = new List<LegoItem>();
            viewModel.View = "Set pricing";
            viewModel.AmountToDisplay = 10;
            viewModel.Type = 2;
            storedViewModel.Filter = string.Empty;
            startingOver = true;
            this.LoadFavorites();
            this.LoadLookups(viewModel);
            this.SetLookups(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult MasterList(InventoryViewModel viewModel)
        {
            this.StoreViewModelData(viewModel);
            this.IncrementPaging(viewModel);
            this.SetChosenList(viewModel);
            this.SetLookups(viewModel);
            this.SetLegoItems(viewModel);
            this.PlugInDelegates(viewModel);
            this.ResetCount(viewModel);
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        [HttpGet]
        public string LoadChosenList(string listId)
        {
            string result = "chosen list set";

            try
            {
                LegoList chosenList = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).FirstOrDefault(l => l.Id == int.Parse(listId));
                storedViewModel.ChosenList = chosenList;
                this.LoadFavorites();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }

        [HttpGet]
        public string SetPrice(string id, string price)
        {
            string result = "price set";

            try
            {
                LegoItem chosenItem = this.context.LegoItems.OrderBy(l => l.Description).FirstOrDefault(l => l.Id == int.Parse(id));
                chosenItem.Price = decimal.Parse(price);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }

        private bool CheckFavorite(int id)
        {
            return favorites.Any(i => i.Id == id);
        }

        private decimal CheckPrice(int id)
        {
            return this.pricingHelper.GetSuggestedPrice(id);
        }

        private void SetTotals()
        {
            IQueryable<LegoItem> itemSource;

            if (storedViewModel.View == "Favorites")
            {
                itemSource = favorites.AsQueryable();
            }
            else
            {
                itemSource = this.context.LegoItems.Include("LegoColor").Include("LegoCategory");
            }

            int itemCount;

            if (!String.IsNullOrEmpty(storedViewModel.Filter))
            {
                itemCount = itemSource.Where(li => li.Description.ToLower().Contains(storedViewModel.Filter.ToLower())
                                                                         || li.BrickLinkId.ToLower().Contains(storedViewModel.Filter.ToLower())
                                                                         || li.LegoColor.Name.ToLower().Contains(storedViewModel.Filter.ToLower())
                                                                         || li.LegoCategory.Name.ToLower().Contains(storedViewModel.Filter.ToLower())
                                                                         && li.LegoTypeId == storedViewModel.Type).Count();
            }
            else
            {
                if (storedViewModel.View == "Favorites")
                {
                    itemCount = itemSource.Count();
                }
                else
                {
                    itemCount = 0;
                }
            }

            storedViewModel.TotalResults = itemCount;

            storedViewModel.TotalPages = Math.Ceiling((double)storedViewModel.TotalResults / (double)storedViewModel.AmountToDisplay);
        }

        private void ResetCount(InventoryViewModel viewModel)
        {
            if (viewModel.LegoItems != null)
            {
                if (viewModel.LegoItems.Count() < viewModel.AmountToDisplay)
                {
                    viewModel.AmountToDisplay = viewModel.LegoItems.Count();
                }
            }
        }

        private void SetChosenList(InventoryViewModel viewModel)
        {
            viewModel.ChosenList = storedViewModel.ChosenList;
        }

        private void SetLegoItems(InventoryViewModel viewModel)
        {
            if (!String.IsNullOrEmpty(viewModel.Filter))
            {
                if (viewModel.PageNumber == 1)
                {
                    viewModel.LegoItems = this.context.LegoItems.Where(li => li.Description.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.BrickLinkId.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoColor.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoCategory.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         && li.LegoTypeId == viewModel.Type).Take(viewModel.AmountToDisplay).ToList();
                }
                else
                {
                    int leftover = this.context.LegoItems.Count() - (viewModel.PageNumber * viewModel.AmountToDisplay);

                    if (viewModel.AmountToDisplay > leftover)
                    {
                        viewModel.AmountToDisplay = leftover;
                    }

                    viewModel.LegoItems = this.context.LegoItems.Where(li => li.Description.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.BrickLinkId.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoColor.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoCategory.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         && li.LegoTypeId == viewModel.Type).Skip(viewModel.AmountToDisplay * (viewModel.PageNumber - 1)).Take(viewModel.AmountToDisplay).ToList();
                }
            }
            else
            {
                viewModel.LegoItems = new List<LegoItem>();
            }
        }

        private void SetFavorites(InventoryViewModel viewModel)
        {
            if (!String.IsNullOrEmpty(viewModel.Filter))
            {
                if (viewModel.PageNumber == 1)
                {
                    viewModel.LegoItems = favorites.Where(li => li.Description.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.BrickLinkId.ToLower().Contains(viewModel.Filter)
                                                                         || li.LegoColor.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoCategory.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         && li.LegoTypeId == viewModel.Type).Take(viewModel.AmountToDisplay).ToList();
                }
                else
                {
                    int leftover = favorites.Count() - (viewModel.PageNumber * viewModel.AmountToDisplay);

                    if (viewModel.AmountToDisplay > leftover)
                    {
                        viewModel.AmountToDisplay = leftover;
                    }

                    List<LegoItem> legoItems = favorites.Where(li => li.Description.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.BrickLinkId.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoColor.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         || li.LegoCategory.Name.ToLower().Contains(viewModel.Filter.ToLower())
                                                                         && li.LegoTypeId == viewModel.Type).Skip(viewModel.AmountToDisplay * (viewModel.PageNumber - 1)).Take(viewModel.AmountToDisplay).ToList();
                    viewModel.LegoItems = legoItems;
                }
            }
            else
            {
                viewModel.LegoItems = favorites.Skip(viewModel.AmountToDisplay * (viewModel.PageNumber - 1)).Take(viewModel.AmountToDisplay).ToList();
            }

            foreach (LegoItem li in viewModel.LegoItems)
            {
                li.CheckPrice = null;
                li.CheckPrice += this.CheckPrice;
            }
        }

        private void StoreViewModelData(InventoryViewModel viewModel)
        {
            storedViewModel.View = viewModel.View;
            storedViewModel.AmountToDisplay = viewModel.AmountToDisplay;
            storedViewModel.Type = viewModel.Type;

            if (storedViewModel.Filter != viewModel.Filter)
            {
                filterChanged = true;
            }
            else
            {
                filterChanged = false;
            }

            storedViewModel.Filter = viewModel.Filter;
        }

        private void IncrementPaging(InventoryViewModel viewModel)
        {
            if (filterChanged || startingOver)
            {
                storedViewModel.PageNumber = 0;
            }

            if (storedViewModel.PageNumber != 0 && viewModel.Previous == null)
            {
                storedViewModel.PageNumber += viewModel.Counter;
                viewModel.PageNumber = storedViewModel.PageNumber;
                startingOver = false;
            }
            if (storedViewModel.PageNumber != 0 && viewModel.Previous == "Previous")
            {
                storedViewModel.PageNumber -= viewModel.Counter;
                viewModel.PageNumber = storedViewModel.PageNumber;
                startingOver = false;
            }
            else if (storedViewModel.PageNumber == 0)
            {
                storedViewModel.PageNumber = 1;
                viewModel.PageNumber = storedViewModel.PageNumber;
                startingOver = false;
            }
        }

        private void LoadFavorites()
        {
            LegoItem legoItem;

            favorites = new List<LegoItem>();

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);

            LegoList favoriteList = null;

            if (storedViewModel.ChosenList != null && storedViewModel.ChosenList.Id != 7)
            {
                favoriteList = storedViewModel.ChosenList;
            }

            if (favoriteList != null)
            {
                foreach (ListBridge lb in favoriteList.ListBridges)
                {
                    legoItem = this.context.LegoItems.Include("LegoCategory").FirstOrDefault(li => li.Id == lb.LegoItemId);

                    favorites.Add(legoItem);
                }
            }
        }

        private void LoadLookups(InventoryViewModel viewModel)
        {
            storedViewModel.LegoColors = this.context.LegoColors.ToList();
            storedViewModel.LegoTypes = this.context.LegoTypes.ToList();
            storedViewModel.PricingMethods = this.context.PricingMethods.ToList();

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);

            storedViewModel.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(lb => lb.Name).Where(l => l.ApplicationUserId == currentUser.Id || l.Id == 7).ToList();
        }

        private void PlugInDelegates(InventoryViewModel viewModel)
        {
            foreach (LegoItem li in viewModel.LegoItems)
            {
                li.CheckFavorite = null;
                li.CheckFavorite += this.CheckFavorite;
                li.CheckPrice = null;
                li.CheckPrice += this.CheckPrice;
            }
        }

        private void SetLookups(InventoryViewModel viewModel)
        {
            viewModel.LegoColors = storedViewModel.LegoColors;
            viewModel.LegoLists = storedViewModel.LegoLists;
            viewModel.LegoTypes = storedViewModel.LegoTypes;
            viewModel.PricingMethods = storedViewModel.PricingMethods;
        }
    }
}
