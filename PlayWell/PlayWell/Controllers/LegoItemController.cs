using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PlayWell.Data;
using PlayWell.Models;
using PlayWell.Helper;
using Microsoft.EntityFrameworkCore;

namespace PlayWell.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LegoItemController : Controller
    {
        private readonly ApplicationDbContext context;

        private PricingHelper pricingHelper;

        public LegoItemController(ApplicationDbContext context)
        {
            this.context = context;
            this.pricingHelper = new PricingHelper(context);
        }

        [HttpGet("UpdatePricingMethod")]
        public string UpdatePricingMethod(string id, string pricingMethodId)
        {
            LegoItem legoItem = this.context.LegoItems.FirstOrDefault(li => li.Id == int.Parse(id));

            if (legoItem != null)
            {
                legoItem.PricingMethodId = int.Parse(pricingMethodId);
            }

            this.context.SaveChanges();

            decimal price = this.pricingHelper.GetSuggestedPrice(legoItem.Id);

            return price.ToString();
        }
    }
}
