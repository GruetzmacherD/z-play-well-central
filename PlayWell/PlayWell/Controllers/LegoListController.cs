using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PlayWell.Data;
using PlayWell.Models;
using PlayWell.Models.ListViewModels;
using Microsoft.AspNetCore.Authorization;
using PlayWell.Models.InventoryViewModels;

namespace PlayWell.Controllers
{
    [Authorize]
    public class LegoListController : Controller
    {
        private readonly ApplicationDbContext context;

        public LegoListController(ApplicationDbContext context)
        {
            this.context = context;    
        }

        // GET: LegoList
        public IActionResult Index()
        {
            InventoryViewModel viewModel = new InventoryViewModel();
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            viewModel.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            ViewBag.LegoLists = viewModel.LegoLists;

            return View(viewModel);
        }

        // GET: LegoList/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var legoList = await this.context.LegoLists
                .Include(l => l.ApplicationUser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (legoList == null)
            {
                return NotFound();
            }

            return View(legoList);
        }

        // GET: LegoList/Create
        public IActionResult Create()
        {
            ViewData["ApplicationUserId"] = new SelectList(this.context.Users, "Id", "Id");
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View();
        }

        // POST: LegoList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApplicationUserId,LastModified,Name")] LegoList legoList)
        {
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);

            if (ModelState.IsValid)
            {
                if (currentUser != null)
                {
                    legoList.ApplicationUserId = currentUser.Id;
                }
                this.context.Add(legoList);
                await this.context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ApplicationUserId"] = new SelectList(this.context.Users, "Id", "Id", legoList.ApplicationUserId);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View(legoList);
        }

        // GET: LegoList/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var legoList = await this.context.LegoLists.SingleOrDefaultAsync(m => m.Id == id);
            if (legoList == null)
            {
                return NotFound();
            }
            ViewData["ApplicationUserId"] = new SelectList(this.context.Users, "Id", "Id", legoList.ApplicationUserId);
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View(legoList);
        }

        // POST: LegoList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ApplicationUserId,LastModified,Name")] LegoList legoList)
        {
            if (id != legoList.Id)
            {
                return NotFound();
            }

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);

            if (ModelState.IsValid)
            {
                if (currentUser != null)
                {
                    legoList.ApplicationUserId = currentUser.Id;
                }

                try
                {
                    this.context.Update(legoList);
                    await this.context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LegoListExists(legoList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ApplicationUserId"] = new SelectList(this.context.Users, "Id", "Id", legoList.ApplicationUserId);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View(legoList);
        }

        // GET: LegoList/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var legoList = await this.context.LegoLists
                .Include(l => l.ApplicationUser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (legoList == null)
            {
                return NotFound();
            }

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View(legoList);
        }

        // POST: LegoList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var legoList = await this.context.LegoLists.SingleOrDefaultAsync(m => m.Id == id);
            this.context.LegoLists.Remove(legoList);
            await this.context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LegoListExists(int id)
        {
            return this.context.LegoLists.Any(e => e.Id == id);
        }
    }
}
