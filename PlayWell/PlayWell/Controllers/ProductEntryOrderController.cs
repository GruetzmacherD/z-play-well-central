using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PlayWell.Models;
using PlayWell.Models.ProductOrderEntryViewModels;
using PlayWell.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace PlayWell.Controllers
{
    [Authorize]
    public class ProductEntryOrderController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> _manager;

        public ProductEntryOrderController(ApplicationDbContext context, UserManager<ApplicationUser> manager)
        {
            this.context = context;
            this._manager = manager;
        }

        public IActionResult Index()
        {

            ApplicationUser user = this.GetUserByEmail(User.Identity.Name);

            ProductOrderEntryViewModel viewModel = new ProductOrderEntryViewModel();
            viewModel.Conditions = new List<Condition>();
            viewModel.Conditions = context.Conditions.ToList();
            viewModel.Users = context.Users.ToList();
            viewModel.SearchResult = new List<LegoItem>();
            //viewModel.Warehouses = context.Warehouses.ToList();
            viewModel.Warehouses = new List<Warehouse>();
            if(user != null)
            {
                viewModel.User = user;
            }

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ProductOrderEntryViewModel viewModel)
        {
            if(viewModel.SearchString != null)
            {

                viewModel.SearchResult = context.LegoItems.Where(li => li.Description.Contains(viewModel.SearchString)
                                                      || li.BrickLinkId.Contains(viewModel.SearchString)
                                                      || li.LegoColor.Name.Contains(viewModel.SearchString)
                                                      || li.LegoCategory.Name.Contains(viewModel.SearchString)).ToList();
            }
            else if(viewModel.ItemToAdd != null)
            {
                // Do something
            }

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();

            return View(viewModel);
        }

        public IActionResult Invoice()
        {
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();

            return View();
        }

        public IActionResult SeeAllInvoices()
        {

            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();
            return View();
        }

        public IActionResult InventorySearch()
        {
            ApplicationUser currentUser = this.context.GetCurrentUser(HttpContext.User.Identity.Name);
            ViewBag.LegoLists = this.context.LegoLists.Include("ListBridges").OrderBy(l => l.Name).Where(l => l.ApplicationUserId == currentUser.Id).ToList();

            return View();
        }

        private ApplicationUser GetUserByEmail(string email)
        {
            return this.context.Users.SingleOrDefault(u => u.Email == email);
        }
    }
}