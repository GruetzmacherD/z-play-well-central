﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlayWell.Models;
using PlayWell.Models.Import;

namespace PlayWell.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private static List<LegoItem> legoItemsList;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Condition> Conditions { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<ForSaleItem> ForSaleItems { get; set; }

        public DbSet<Import_Item> ImportItems { get; set; }

        public DbSet<Import_Composition> ImportCompositions { get; set; }

        public DbSet<InventoryItem> InventoryItems { get; set; }

        public DbSet<InventoryLocation> InventoryLocations { get; set; }

        public DbSet<ItemListing> ItemListings { get; set; }

        public DbSet<LegoCategory> LegoCategories { get; set; }

        public DbSet<LegoColor> LegoColors { get; set; }

        public DbSet<PricingMethod> PricingMethods { get; set; }

        public DbSet<LegoItem> LegoItems { get; set; }

        public List<LegoItem> LegoItemsList
        {
            get
            {
                return legoItemsList;
            }
            set
            {
                legoItemsList = value;
            }
        }

        public DbSet<LegoList> LegoLists { get; set; }

        public DbSet<LegoType> LegoTypes { get; set; }

        public DbSet<ListBridge> ListBridges { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }

        public DbSet<PurchaseOrderLine> PurchaseOrderLines { get; set; }

        public DbSet<OrderStatus> OrderStatuses { get; set; }

        public DbSet<SalesChannel> SalesChannels { get; set; }

        public DbSet<SetBridge> SetBridges { get; set; }

        public DbSet<SoldItem> SoldItems { get; set; }

        public DbSet<Vendor> Vendors { get; set; }

        public DbSet<VendorType> VendorTypes { get; set; }

        public DbSet<Warehouse> Warehouses { get; set; }

        public ApplicationUser GetCurrentUser(string email)
        {
            return this.Users.FirstOrDefault(u => u.Email == email);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // builder.Entity<LegoColor>().ToTable("LegoColor");

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
