﻿using PlayWell.Data;
using PlayWell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class DefaultPricer : IPricer
    {
        private readonly ApplicationDbContext context;

        public DefaultPricer(ApplicationDbContext context)
        {
            this.context = context;
        }

        public decimal GetPrice(LegoItem legoItem)
        {
            return legoItem.Price;
        }
    }
}
