﻿using PlayWell.Data;
using PlayWell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class DownPricer : IPricer
    {
        private readonly ApplicationDbContext context;

        public DownPricer(ApplicationDbContext context)
        {
            this.context = context;
        }

        public decimal GetPrice(LegoItem legoItem)
        {
            return Math.Round(legoItem.Price * 0.95m, 2);
        }
    }
}
