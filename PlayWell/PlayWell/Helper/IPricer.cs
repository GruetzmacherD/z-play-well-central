﻿using PlayWell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public interface IPricer
    {
        decimal GetPrice(LegoItem legoItem);
    }
}
