﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using PlayWell.Data;
using PlayWell.Models;
using PlayWell.Models.Import;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class Importer
    {
        private readonly ApplicationDbContext context;

        private LogWriter logWriter;

        public Importer(ApplicationDbContext context, IHostingEnvironment environment)
        {
            this.context = context;
            this.logWriter = new LogWriter(context, environment);
        }

        public void RemoveDuplicateParts()
        {
            Import_Item item = null;
            IQueryable<Import_Item> matchingItems = null;
            List<Import_Item> toBeRemoved = new List<Import_Item>();
            string bricklinktest = string.Empty;
            string colortest = string.Empty;
            int y = 0;

            List<Import_Item> importItemsCopy = this.context.ImportItems.Where(i => i.type == "P" && i.color == 0).ToList();

            importItemsCopy.OrderBy(i => i.bricklink_id);

            this.context.ImportItems.OrderBy(i => i.bricklink_id);

            foreach (Import_Item i in importItemsCopy)
            {
                matchingItems = this.context.ImportItems.Where(x => x.bricklink_id == i.bricklink_id);

                if (matchingItems.Count() > 1)
                {
                    foreach (Import_Item m in matchingItems)
                    {
                        if (m.type == "P" && m.color == 0)
                        {
                            toBeRemoved.Add(m);

                            //this.context.ImportItems.Remove(m);

                            //this.context.SaveChanges();

                            y++;
                        }
                    }
                }
            }
        }

        public void SetBridgeDataImport()
        {
            List<SetBridge> test = new List<SetBridge>();

            this.context.ImportCompositions.OrderBy(i => i.contained_id);

            this.context.SetBridges.OrderBy(s => s.SetId);

            this.context.LegoItems.OrderBy(l => l.BrickLinkId);

            int x = 0;

            // List<LegoItem> legoItems = this.context.LegoItems.Include(l => l.LegoType).Include(l => l.LegoColor).ToList();

            foreach (Import_Composition ic in this.context.ImportCompositions)
            {
                if (x >= 0)
                {
                    Import_Item containedItem = this.context.ImportItems.FirstOrDefault(ii => ii.new_new_id == ic.contained_id);
                    Import_Item containerItem = this.context.ImportItems.FirstOrDefault(ii => ii.new_new_id == ic.container_id);

                    int setId = this.context.LegoItems.FirstOrDefault(l => l.BrickLinkId == containerItem.bricklink_id && l.LegoColor.BrickLinkId == containerItem.color.ToString() && l.LegoType.BrickLinkId == containerItem.type).Id;
                    int itemId = this.context.LegoItems.FirstOrDefault(l => l.BrickLinkId == containedItem.bricklink_id && l.LegoColor.BrickLinkId == containedItem.color.ToString() && l.LegoType.BrickLinkId == containedItem.type).Id;

                    ////foreach (LegoItem l in legoItems)
                    ////{
                    ////    if (l.BrickLinkId == containedItem.bricklink_id && l.LegoColor.BrickLinkId == containedItem.color.ToString() && l.LegoType.BrickLinkId == containedItem.type)
                    ////    {
                    ////        itemId = l.Id;
                    ////        break;
                    ////    }
                    ////}

                    if (itemId == 0)
                    {
                        break;
                    }

                    if (this.context.SetBridges.Any(s => s.SetId == setId && s.ItemId == itemId) == false)
                    {
                        SetBridge setBridge = new SetBridge
                        {
                            SetId = setId,
                            ItemId = itemId,
                            Quantity = ic.quantity,
                            Alternate = ic.alternate,
                            CounterPart = ic.counter_part,
                            Extra = ic.extra,
                            MatchId = ic.match_id
                        };

                        this.context.SetBridges.Add(setBridge);

                        if (x != 0 && x % 200 == 0)
                        {
                            context.SaveChanges();

                            this.logWriter.WriteLog("SetBridge_Log", x.ToString() + " SetBridges imported at " + DateTime.Now.ToString());
                        }

                        x++;
                    }
                }
            }
        }
    }
}
