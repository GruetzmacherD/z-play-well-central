﻿using Microsoft.AspNetCore.Hosting;
using PlayWell.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class LogWriter
    {
        private readonly ApplicationDbContext context;

        private IHostingEnvironment environment;

        private string webRootPath;

        private string logPath = @"\logs\";

        public LogWriter(ApplicationDbContext context, IHostingEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
            this.webRootPath = this.environment.WebRootPath;
        }

        public void WriteLog(string title, string message)
        {
            var filePath = this.webRootPath + this.logPath + title + ".txt";
            using (var writer = System.IO.File.CreateText(filePath))
            {
                writer.WriteLine(message.ToString()); //or .Write(), if you wish
            }
        }
    }
}
