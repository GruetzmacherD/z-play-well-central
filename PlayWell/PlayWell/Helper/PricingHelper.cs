﻿using PlayWell.Data;
using PlayWell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class PricingHelper
    {
        private readonly ApplicationDbContext context;

        private IPricer pricer;

        public PricingHelper(ApplicationDbContext context)
        {
            this.context = context;
        }

        public decimal GetSuggestedPrice(int itemId)
        {
            LegoItem legoItem = this.context.LegoItems.FirstOrDefault(li => li.Id == itemId);

            if (legoItem != null)
            {
                if (legoItem.PricingMethodId == 1)
                {
                    pricer = new DefaultPricer(this.context);
                }
                else if (legoItem.PricingMethodId == 2)
                {
                    pricer = new UpPricer(this.context);
                }
                else if (legoItem.PricingMethodId == 3)
                {
                    pricer = new DownPricer(this.context);
                }
            }

            return pricer.GetPrice(legoItem);
        }
    }
}
