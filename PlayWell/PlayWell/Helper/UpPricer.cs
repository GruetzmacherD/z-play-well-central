﻿using PlayWell.Data;
using PlayWell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Helper
{
    public class UpPricer : IPricer
    {
        private readonly ApplicationDbContext context;

        public UpPricer(ApplicationDbContext context)
        {
            this.context = context;
        }

        public decimal GetPrice(LegoItem legoItem)
        {
            return Math.Round(legoItem.Price * 1.05m, 2);
        }
    }
}
