﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoItems_LegoColor_LegoColorId",
                table: "LegoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LegoColor",
                table: "LegoColor");

            migrationBuilder.RenameTable(
                name: "LegoColor",
                newName: "LegoColors");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LegoColors",
                table: "LegoColors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoItems_LegoColors_LegoColorId",
                table: "LegoItems",
                column: "LegoColorId",
                principalTable: "LegoColors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoItems_LegoColors_LegoColorId",
                table: "LegoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LegoColors",
                table: "LegoColors");

            migrationBuilder.RenameTable(
                name: "LegoColors",
                newName: "LegoColor");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LegoColor",
                table: "LegoColor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoItems_LegoColor_LegoColorId",
                table: "LegoItems",
                column: "LegoColorId",
                principalTable: "LegoColor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
