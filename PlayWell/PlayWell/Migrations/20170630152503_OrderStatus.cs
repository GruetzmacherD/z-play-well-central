﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PlayWell.Migrations
{
    public partial class OrderStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendors_StoreTypes_StoreTypeId",
                table: "Vendors");

            migrationBuilder.DropTable(
                name: "StoreTypes");

            migrationBuilder.AddColumn<int>(
                name: "OrderStatusId",
                table: "PurchaseOrderLines",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OrderStatusId",
                table: "PurchaseOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "OrderStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VendorTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrderLines_OrderStatusId",
                table: "PurchaseOrderLines",
                column: "OrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_OrderStatusId",
                table: "PurchaseOrders",
                column: "OrderStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrders_OrderStatuses_OrderStatusId",
                table: "PurchaseOrders",
                column: "OrderStatusId",
                principalTable: "OrderStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrderLines_OrderStatuses_OrderStatusId",
                table: "PurchaseOrderLines",
                column: "OrderStatusId",
                principalTable: "OrderStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Vendors_VendorTypes_StoreTypeId",
                table: "Vendors",
                column: "StoreTypeId",
                principalTable: "VendorTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrders_OrderStatuses_OrderStatusId",
                table: "PurchaseOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrderLines_OrderStatuses_OrderStatusId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropForeignKey(
                name: "FK_Vendors_VendorTypes_StoreTypeId",
                table: "Vendors");

            migrationBuilder.DropTable(
                name: "OrderStatuses");

            migrationBuilder.DropTable(
                name: "VendorTypes");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrderLines_OrderStatusId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrders_OrderStatusId",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "OrderStatusId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropColumn(
                name: "OrderStatusId",
                table: "PurchaseOrders");

            migrationBuilder.CreateTable(
                name: "StoreTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreTypes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Vendors_StoreTypes_StoreTypeId",
                table: "Vendors",
                column: "StoreTypeId",
                principalTable: "StoreTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
