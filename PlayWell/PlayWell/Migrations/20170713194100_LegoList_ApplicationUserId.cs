﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class LegoList_ApplicationUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoLists_AspNetUsers_ApplicationUserId1",
                table: "LegoLists");

            migrationBuilder.DropIndex(
                name: "IX_LegoLists_ApplicationUserId1",
                table: "LegoLists");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "LegoLists");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "LegoLists",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_LegoLists_ApplicationUserId",
                table: "LegoLists",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoLists_AspNetUsers_ApplicationUserId",
                table: "LegoLists",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoLists_AspNetUsers_ApplicationUserId",
                table: "LegoLists");

            migrationBuilder.DropIndex(
                name: "IX_LegoLists_ApplicationUserId",
                table: "LegoLists");

            migrationBuilder.AlterColumn<int>(
                name: "ApplicationUserId",
                table: "LegoLists",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "LegoLists",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LegoLists_ApplicationUserId1",
                table: "LegoLists",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoLists_AspNetUsers_ApplicationUserId1",
                table: "LegoLists",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
