﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PlayWell.Migrations
{
    public partial class Removed_PricingMethod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoItems_PricingMethod_PricingMethodId",
                table: "LegoItems");

            migrationBuilder.DropTable(
                name: "PricingMethod");

            migrationBuilder.DropIndex(
                name: "IX_LegoItems_PricingMethodId",
                table: "LegoItems");

            migrationBuilder.DropColumn(
                name: "PricingMethodId",
                table: "LegoItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PricingMethodId",
                table: "LegoItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PricingMethod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PricingMethod", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LegoItems_PricingMethodId",
                table: "LegoItems",
                column: "PricingMethodId");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoItems_PricingMethod_PricingMethodId",
                table: "LegoItems",
                column: "PricingMethodId",
                principalTable: "PricingMethod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
