﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class PricingMethods2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoItems_PricingMethod_PricingMethodId",
                table: "LegoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PricingMethod",
                table: "PricingMethod");

            migrationBuilder.RenameTable(
                name: "PricingMethod",
                newName: "PricingMethods");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PricingMethods",
                table: "PricingMethods",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoItems_PricingMethods_PricingMethodId",
                table: "LegoItems",
                column: "PricingMethodId",
                principalTable: "PricingMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LegoItems_PricingMethods_PricingMethodId",
                table: "LegoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PricingMethods",
                table: "PricingMethods");

            migrationBuilder.RenameTable(
                name: "PricingMethods",
                newName: "PricingMethod");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PricingMethod",
                table: "PricingMethod",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LegoItems_PricingMethod_PricingMethodId",
                table: "LegoItems",
                column: "PricingMethodId",
                principalTable: "PricingMethod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
