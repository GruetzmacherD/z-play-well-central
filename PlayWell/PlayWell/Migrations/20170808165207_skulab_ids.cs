﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class skulab_ids : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedWarehouseId",
                table: "Warehouses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedId",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedWareHouseId",
                table: "Locations",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost",
                table: "InventoryItems",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedWarehouseId",
                table: "Warehouses");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedWareHouseId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryItems");

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost",
                table: "InventoryItems",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
