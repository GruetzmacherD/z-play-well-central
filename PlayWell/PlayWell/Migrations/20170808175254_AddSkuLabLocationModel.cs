﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class AddSkuLabLocationModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "PurchaseOrderLines",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedLocationId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SkuLabLocation",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccountId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    WarehouseId = table.Column<string>(nullable: true),
                    WarehouseId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SkuLabLocation_Warehouses_WarehouseId1",
                        column: x => x.WarehouseId1,
                        principalTable: "Warehouses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrderLines_SkuLabLocationId",
                table: "PurchaseOrderLines",
                column: "SkuLabLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryLocations_SkuLabLocationId",
                table: "InventoryLocations",
                column: "SkuLabLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_SkuLabLocationId",
                table: "AspNetUsers",
                column: "SkuLabLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SkuLabLocation_WarehouseId1",
                table: "SkuLabLocation",
                column: "WarehouseId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_SkuLabLocation_SkuLabLocationId",
                table: "AspNetUsers",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryLocations_SkuLabLocation_SkuLabLocationId",
                table: "InventoryLocations",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrderLines_SkuLabLocation_SkuLabLocationId",
                table: "PurchaseOrderLines",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_SkuLabLocation_SkuLabLocationId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_InventoryLocations_SkuLabLocation_SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrderLines_SkuLabLocation_SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropTable(
                name: "SkuLabLocation");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrderLines_SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropIndex(
                name: "IX_InventoryLocations_SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_SkuLabLocationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "AspNetUsers");
        }
    }
}
