﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class skulabs_sync1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedWareHouseId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedId",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedWareHouseId",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedLocationId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabsGeneratedItemId",
                table: "InventoryItems",
                nullable: true);
        }
    }
}
