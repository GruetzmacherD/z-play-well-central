﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class test81617 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_SkuLabLocation_SkuLabLocationId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_InventoryLocations_SkuLabLocation_SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrderLines_SkuLabLocation_SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropForeignKey(
                name: "FK_SkuLabLocation_Warehouses_WarehouseId1",
                table: "SkuLabLocation");

            migrationBuilder.DropIndex(
                name: "IX_SkuLabLocation_WarehouseId1",
                table: "SkuLabLocation");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrderLines_SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropIndex(
                name: "IX_InventoryLocations_SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_SkuLabLocationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "WarehouseId1",
                table: "SkuLabLocation");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "PurchaseOrderLines");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "InventoryLocations");

            migrationBuilder.DropColumn(
                name: "SkuLabLocationId",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WarehouseId1",
                table: "SkuLabLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "PurchaseOrderLines",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "InventoryLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkuLabLocationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SkuLabLocation_WarehouseId1",
                table: "SkuLabLocation",
                column: "WarehouseId1");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrderLines_SkuLabLocationId",
                table: "PurchaseOrderLines",
                column: "SkuLabLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryLocations_SkuLabLocationId",
                table: "InventoryLocations",
                column: "SkuLabLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_SkuLabLocationId",
                table: "AspNetUsers",
                column: "SkuLabLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_SkuLabLocation_SkuLabLocationId",
                table: "AspNetUsers",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryLocations_SkuLabLocation_SkuLabLocationId",
                table: "InventoryLocations",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrderLines_SkuLabLocation_SkuLabLocationId",
                table: "PurchaseOrderLines",
                column: "SkuLabLocationId",
                principalTable: "SkuLabLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SkuLabLocation_Warehouses_WarehouseId1",
                table: "SkuLabLocation",
                column: "WarehouseId1",
                principalTable: "Warehouses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
