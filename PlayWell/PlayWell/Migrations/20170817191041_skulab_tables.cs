﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class skulab_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SkuLabCustomFields");

            migrationBuilder.DropTable(
                name: "SkuLabLocation");

            migrationBuilder.DropTable(
                name: "SkuLabInventory");

            migrationBuilder.DropTable(
                name: "SkuLabWarehouse");

            migrationBuilder.DropTable(
                name: "SkuLabItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SkuLabItems",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Cost = table.Column<decimal>(nullable: true),
                    Height = table.Column<int>(nullable: true),
                    Length = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Retail = table.Column<decimal>(nullable: true),
                    Sku = table.Column<string>(nullable: true),
                    Weight = table.Column<double>(nullable: true),
                    WeightUnit = table.Column<string>(nullable: true),
                    Width = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkuLabLocation",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccountId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    WarehouseId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabLocation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkuLabInventory",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    LocationId = table.Column<string>(nullable: true),
                    QuantityOnHand = table.Column<int>(nullable: true),
                    SkuLabsGeneratedItemId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabInventory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkuLabWarehouse",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabWarehouse", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkuLabCustomFields",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    BrickLinkId = table.Column<string>(nullable: true),
                    SkuLabItemId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkuLabCustomFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SkuLabCustomFields_SkuLabItems_SkuLabItemId",
                        column: x => x.SkuLabItemId,
                        principalTable: "SkuLabItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SkuLabCustomFields_SkuLabItemId",
                table: "SkuLabCustomFields",
                column: "SkuLabItemId");
        }
    }
}
