﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayWell.Migrations
{
    public partial class New_SetBridge_Properties1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CounterPart",
                table: "SetBridges",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Extra",
                table: "SetBridges",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MatchId",
                table: "SetBridges",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CounterPart",
                table: "SetBridges");

            migrationBuilder.DropColumn(
                name: "Extra",
                table: "SetBridges");

            migrationBuilder.DropColumn(
                name: "MatchId",
                table: "SetBridges");
        }
    }
}
