﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PlayWell.Migrations
{
    public partial class new_item_import : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LegoColorBrickLinkId",
                table: "LegoItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LegoTypeBrickLinkId",
                table: "LegoItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ImportItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bricklink_id = table.Column<string>(nullable: true),
                    category = table.Column<int>(nullable: false),
                    color = table.Column<int>(nullable: false),
                    dimx = table.Column<decimal>(nullable: false),
                    dimy = table.Column<decimal>(nullable: false),
                    dimz = table.Column<decimal>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    new_id = table.Column<int>(nullable: false),
                    type = table.Column<string>(nullable: true),
                    weight = table.Column<decimal>(nullable: false),
                    year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImportItems");

            migrationBuilder.DropColumn(
                name: "LegoColorBrickLinkId",
                table: "LegoItems");

            migrationBuilder.DropColumn(
                name: "LegoTypeBrickLinkId",
                table: "LegoItems");
        }
    }
}
