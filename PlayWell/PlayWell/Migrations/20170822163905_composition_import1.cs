﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PlayWell.Migrations
{
    public partial class composition_import1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ImportCompositions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    alternate = table.Column<bool>(nullable: false),
                    contained_id = table.Column<string>(nullable: true),
                    container_id = table.Column<string>(nullable: true),
                    counter_part = table.Column<bool>(nullable: false),
                    extra = table.Column<bool>(nullable: false),
                    match_id = table.Column<int>(nullable: false),
                    quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportCompositions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImportItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bricklink_id = table.Column<string>(nullable: true),
                    category = table.Column<int>(nullable: false),
                    color = table.Column<int>(nullable: false),
                    dimx = table.Column<decimal>(nullable: false),
                    dimy = table.Column<decimal>(nullable: false),
                    dimz = table.Column<decimal>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    new_new_id = table.Column<int>(nullable: false),
                    type = table.Column<string>(nullable: true),
                    weight = table.Column<decimal>(nullable: false),
                    year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImportCompositions");

            migrationBuilder.DropTable(
                name: "ImportItems");
        }
    }
}
