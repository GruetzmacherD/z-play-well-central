﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PlayWell.Data;

namespace PlayWell.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170904220850_for_sale_import3")]
    partial class for_sale_import3
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("PlayWell.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<decimal>("DefaultCommission");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<int?>("LocationId");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.Property<int>("WarehouseId");

                    b.HasKey("Id");

                    b.HasIndex("LocationId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("WarehouseId");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("PlayWell.Models.Condition", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Conditions");
                });

            modelBuilder.Entity("PlayWell.Models.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Countries");
                });

            modelBuilder.Entity("PlayWell.Models.ForSaleItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkLotId");

                    b.Property<int>("ConditionId");

                    b.Property<int>("LegoItemId");

                    b.Property<decimal>("Price");

                    b.Property<int>("Quantity");

                    b.Property<bool>("WillShipToUs");

                    b.HasKey("Id");

                    b.HasIndex("ConditionId");

                    b.HasIndex("LegoItemId");

                    b.ToTable("ForSaleItems");
                });

            modelBuilder.Entity("PlayWell.Models.Import.Import_Composition", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("alternate");

                    b.Property<int>("contained_id");

                    b.Property<int>("container_id");

                    b.Property<bool>("counter_part");

                    b.Property<bool>("extra");

                    b.Property<int>("match_id");

                    b.Property<int>("quantity");

                    b.HasKey("Id");

                    b.ToTable("ImportCompositions");
                });

            modelBuilder.Entity("PlayWell.Models.Import.Import_Item", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("bricklink_id");

                    b.Property<int>("category");

                    b.Property<int>("color");

                    b.Property<decimal?>("dimx");

                    b.Property<decimal?>("dimy");

                    b.Property<decimal?>("dimz");

                    b.Property<string>("name");

                    b.Property<int>("new_new_id");

                    b.Property<string>("type");

                    b.Property<decimal?>("weight");

                    b.Property<int?>("year");

                    b.HasKey("Id");

                    b.ToTable("ImportItems");
                });

            modelBuilder.Entity("PlayWell.Models.InventoryItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkLotId");

                    b.Property<int>("ConditionId");

                    b.Property<decimal?>("Cost");

                    b.Property<int>("LegoItemId");

                    b.Property<int>("QuantityTotal");

                    b.Property<string>("Sku");

                    b.HasKey("Id");

                    b.HasIndex("ConditionId");

                    b.HasIndex("LegoItemId");

                    b.ToTable("InventoryItems");
                });

            modelBuilder.Entity("PlayWell.Models.InventoryLocation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("InventoryItemId");

                    b.Property<int>("LocationId");

                    b.Property<int>("Quantity");

                    b.HasKey("Id");

                    b.HasIndex("InventoryItemId");

                    b.HasIndex("LocationId");

                    b.ToTable("InventoryLocations");
                });

            modelBuilder.Entity("PlayWell.Models.ItemListing", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("InventoryItemId");

                    b.Property<string>("ListingSku");

                    b.Property<decimal>("Price");

                    b.Property<int>("SalesChannelId");

                    b.HasKey("Id");

                    b.HasIndex("InventoryItemId");

                    b.HasIndex("SalesChannelId");

                    b.ToTable("ItemListings");
                });

            modelBuilder.Entity("PlayWell.Models.LegoCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("LegoCategories");
                });

            modelBuilder.Entity("PlayWell.Models.LegoColor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("LegoColors");
                });

            modelBuilder.Entity("PlayWell.Models.LegoItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkId");

                    b.Property<DateTime?>("DateFirstSold");

                    b.Property<string>("Description");

                    b.Property<double?>("DimensionX");

                    b.Property<double?>("DimensionY");

                    b.Property<double?>("DimensionZ");

                    b.Property<decimal?>("ForSaleAveragePrice");

                    b.Property<int?>("ForSaleQuantityTotal");

                    b.Property<DateTime?>("LastUpdated");

                    b.Property<int>("LegoCategoryId");

                    b.Property<int>("LegoColorId");

                    b.Property<int>("LegoTypeId");

                    b.Property<decimal?>("MSRP");

                    b.Property<int?>("NumberOfForSaleLots");

                    b.Property<int?>("NumberOfSaleTransactions");

                    b.Property<decimal>("Price");

                    b.Property<int>("PricingMethodId");

                    b.Property<decimal?>("SoldAveragePrice");

                    b.Property<int?>("SoldQuantityTotal");

                    b.Property<double?>("Weight");

                    b.Property<int?>("Year");

                    b.HasKey("Id");

                    b.HasIndex("LegoCategoryId");

                    b.HasIndex("LegoColorId");

                    b.HasIndex("LegoTypeId");

                    b.HasIndex("PricingMethodId");

                    b.ToTable("LegoItems");
                });

            modelBuilder.Entity("PlayWell.Models.LegoList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<DateTime>("LastModified");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.ToTable("LegoLists");
                });

            modelBuilder.Entity("PlayWell.Models.LegoType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrickLinkId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("LegoTypes");
                });

            modelBuilder.Entity("PlayWell.Models.ListBridge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LegoItemId");

                    b.Property<int>("LegoListId");

                    b.HasKey("Id");

                    b.HasIndex("LegoItemId");

                    b.HasIndex("LegoListId");

                    b.ToTable("ListBridges");
                });

            modelBuilder.Entity("PlayWell.Models.Location", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("WarehouseId");

                    b.HasKey("Id");

                    b.HasIndex("WarehouseId");

                    b.ToTable("Locations");
                });

            modelBuilder.Entity("PlayWell.Models.OrderStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("OrderStatuses");
                });

            modelBuilder.Entity("PlayWell.Models.PricingMethod", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("PricingMethods");
                });

            modelBuilder.Entity("PlayWell.Models.PurchaseOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ApplicationUserId");

                    b.Property<string>("ApplicationUserId1");

                    b.Property<decimal>("CommissionTotal");

                    b.Property<int>("OrderStatusId");

                    b.Property<decimal>("PriceTotal");

                    b.Property<decimal>("ShippingTotal");

                    b.Property<int>("StoreId");

                    b.Property<string>("StoreLocation");

                    b.Property<decimal>("TaxTotal");

                    b.Property<int?>("VendorId");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId1");

                    b.HasIndex("OrderStatusId");

                    b.HasIndex("VendorId");

                    b.ToTable("PurchaseOrders");
                });

            modelBuilder.Entity("PlayWell.Models.PurchaseOrderLine", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Commission");

                    b.Property<int>("ConditionId");

                    b.Property<int>("LegoItemId");

                    b.Property<int>("LocationId");

                    b.Property<int>("OrderStatusId");

                    b.Property<decimal>("Price");

                    b.Property<int>("PurchaseOrderId");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("Shipping");

                    b.Property<decimal>("Tax");

                    b.HasKey("Id");

                    b.HasIndex("ConditionId");

                    b.HasIndex("LegoItemId");

                    b.HasIndex("LocationId");

                    b.HasIndex("OrderStatusId");

                    b.HasIndex("PurchaseOrderId");

                    b.ToTable("PurchaseOrderLines");
                });

            modelBuilder.Entity("PlayWell.Models.SalesChannel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("SalesChannels");
                });

            modelBuilder.Entity("PlayWell.Models.SetBridge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Alternate");

                    b.Property<bool?>("CounterPart");

                    b.Property<bool?>("Extra");

                    b.Property<int>("ItemId");

                    b.Property<int>("MatchId");

                    b.Property<int>("Quantity");

                    b.Property<int>("SetId");

                    b.HasKey("Id");

                    b.HasIndex("ItemId");

                    b.HasIndex("SetId");

                    b.ToTable("SetBridges");
                });

            modelBuilder.Entity("PlayWell.Models.SoldItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BuyerCountryId");

                    b.Property<int>("ConditionId");

                    b.Property<int>("LegoItemId");

                    b.Property<decimal>("Price");

                    b.Property<int>("Quantity");

                    b.Property<DateTime>("SaleDate");

                    b.Property<int>("SellerCountryId");

                    b.HasKey("Id");

                    b.HasIndex("BuyerCountryId");

                    b.HasIndex("ConditionId");

                    b.HasIndex("LegoItemId");

                    b.HasIndex("SellerCountryId");

                    b.ToTable("SoldItems");
                });

            modelBuilder.Entity("PlayWell.Models.Vendor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("Quantity");

                    b.Property<int>("StoreTypeId");

                    b.HasKey("Id");

                    b.HasIndex("StoreTypeId");

                    b.ToTable("Vendors");
                });

            modelBuilder.Entity("PlayWell.Models.VendorType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("VendorTypes");
                });

            modelBuilder.Entity("PlayWell.Models.Warehouse", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Warehouses");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("PlayWell.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("PlayWell.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.ApplicationUser", b =>
                {
                    b.HasOne("PlayWell.Models.Location")
                        .WithMany("ApplicationUsers")
                        .HasForeignKey("LocationId");

                    b.HasOne("PlayWell.Models.Warehouse", "Warehouse")
                        .WithMany("ApplicationUsers")
                        .HasForeignKey("WarehouseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.ForSaleItem", b =>
                {
                    b.HasOne("PlayWell.Models.Condition", "Condition")
                        .WithMany("ForSaleItems")
                        .HasForeignKey("ConditionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoItem", "LegoItem")
                        .WithMany("ForSaleItems")
                        .HasForeignKey("LegoItemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.InventoryItem", b =>
                {
                    b.HasOne("PlayWell.Models.Condition", "Condition")
                        .WithMany("InventoryItems")
                        .HasForeignKey("ConditionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoItem", "LegoItem")
                        .WithMany("InventoryItems")
                        .HasForeignKey("LegoItemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.InventoryLocation", b =>
                {
                    b.HasOne("PlayWell.Models.InventoryItem", "InventoryItem")
                        .WithMany("InventoryLocations")
                        .HasForeignKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.Location", "Location")
                        .WithMany("InventoryLocations")
                        .HasForeignKey("LocationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.ItemListing", b =>
                {
                    b.HasOne("PlayWell.Models.InventoryItem", "InventoryItem")
                        .WithMany()
                        .HasForeignKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.SalesChannel", "SalesChannel")
                        .WithMany()
                        .HasForeignKey("SalesChannelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.LegoItem", b =>
                {
                    b.HasOne("PlayWell.Models.LegoCategory", "LegoCategory")
                        .WithMany("LegoItems")
                        .HasForeignKey("LegoCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoColor", "LegoColor")
                        .WithMany("LegoItems")
                        .HasForeignKey("LegoColorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoType", "LegoType")
                        .WithMany("LegoItems")
                        .HasForeignKey("LegoTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.PricingMethod", "PricingMethod")
                        .WithMany("LegoItems")
                        .HasForeignKey("PricingMethodId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.LegoList", b =>
                {
                    b.HasOne("PlayWell.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("LegoLists")
                        .HasForeignKey("ApplicationUserId");
                });

            modelBuilder.Entity("PlayWell.Models.ListBridge", b =>
                {
                    b.HasOne("PlayWell.Models.LegoItem", "LegoItem")
                        .WithMany()
                        .HasForeignKey("LegoItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoList", "LegoList")
                        .WithMany("ListBridges")
                        .HasForeignKey("LegoListId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.Location", b =>
                {
                    b.HasOne("PlayWell.Models.Warehouse", "Warehouse")
                        .WithMany("Locations")
                        .HasForeignKey("WarehouseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.PurchaseOrder", b =>
                {
                    b.HasOne("PlayWell.Models.ApplicationUser", "ApplicationUser")
                        .WithMany()
                        .HasForeignKey("ApplicationUserId1");

                    b.HasOne("PlayWell.Models.OrderStatus", "OrderStatus")
                        .WithMany("PurchaseOrders")
                        .HasForeignKey("OrderStatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.Vendor", "Vendor")
                        .WithMany()
                        .HasForeignKey("VendorId");
                });

            modelBuilder.Entity("PlayWell.Models.PurchaseOrderLine", b =>
                {
                    b.HasOne("PlayWell.Models.Condition", "Condition")
                        .WithMany("PurchaseOrderLines")
                        .HasForeignKey("ConditionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoItem", "LegoItem")
                        .WithMany("PurchaseOrderLines")
                        .HasForeignKey("LegoItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.Location", "Location")
                        .WithMany("PurchaseOrderLines")
                        .HasForeignKey("LocationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.OrderStatus", "OrderStatus")
                        .WithMany("PurchaseOrderLines")
                        .HasForeignKey("OrderStatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.PurchaseOrder", "PurchaseOrder")
                        .WithMany()
                        .HasForeignKey("PurchaseOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.SetBridge", b =>
                {
                    b.HasOne("PlayWell.Models.LegoItem", "Part")
                        .WithMany("SetParts")
                        .HasForeignKey("ItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoItem", "Set")
                        .WithMany("Sets")
                        .HasForeignKey("SetId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.SoldItem", b =>
                {
                    b.HasOne("PlayWell.Models.Country", "BuyerCountry")
                        .WithMany("Buyers")
                        .HasForeignKey("BuyerCountryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.Condition", "Condition")
                        .WithMany("SoldItems")
                        .HasForeignKey("ConditionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.LegoItem", "LegoItem")
                        .WithMany("SoldItems")
                        .HasForeignKey("LegoItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PlayWell.Models.Country", "SellerCountry")
                        .WithMany("Sellers")
                        .HasForeignKey("SellerCountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PlayWell.Models.Vendor", b =>
                {
                    b.HasOne("PlayWell.Models.VendorType", "StoreType")
                        .WithMany("Vendors")
                        .HasForeignKey("StoreTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
