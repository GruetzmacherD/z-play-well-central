﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlayWell.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<LegoList> LegoLists { get; set; }

        public decimal DefaultCommission { get; set; }

        public int WarehouseId { get; set; }

        public Warehouse Warehouse { get; set;}
    }
}
