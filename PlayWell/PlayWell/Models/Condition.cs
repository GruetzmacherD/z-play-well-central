﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class Condition
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<ForSaleItem> ForSaleItems { get; set; }

        public ICollection<SoldItem> SoldItems { get; set; }

        public ICollection<InventoryItem> InventoryItems { get; set; }

        public ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
    }
}
