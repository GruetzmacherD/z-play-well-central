﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class Country
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        [InverseProperty("BuyerCountry")]
        public ICollection<SoldItem> Buyers { get; set; }

        [InverseProperty("SellerCountry")]
        public ICollection<SoldItem> Sellers { get; set; }
    }
}
