﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class ForSaleItem
    {
        public int Id { get; set; }

        public int LegoItemId { get; set; }

        public LegoItem LegoItem { get; set; }

        public int ConditionId { get; set; }

        public Condition Condition { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public bool WillShipToUs { get; set; }

        public string BrickLinkLotId { get; set; }
    }
}
