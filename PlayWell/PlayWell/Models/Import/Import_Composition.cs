﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.Import
{
    public class Import_Composition
    {
        public int Id { get; set; }

        public int container_id { get; set; }

        public int contained_id { get; set; }

        public bool alternate { get; set; }

        public int quantity { get; set; }

        public bool counter_part { get; set; }

        public bool extra { get; set; }

        public int match_id { get; set; }
    }
}
