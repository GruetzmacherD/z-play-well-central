﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.Import
{
    public class Import_ForSale
    {
        public int Id { get; set; }

        public string item_id { get; set; }

        public string type { get; set; }

        public string cond { get; set; }

        public int quantity { get; set; }

        public decimal price { get; set; }

        public bool will_ship { get; set; }

        public int color { get; set; }
    }
}
