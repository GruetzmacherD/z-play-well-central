﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.Import
{
    public class Import_Item
    {
        public int Id { get; set; }

        public int new_new_id { get; set; }

        public string type { get; set; }

        public string bricklink_id { get; set; }

        public string name { get; set; }

        public int category { get; set; }

        public int? year { get; set; }

        public decimal? weight { get; set; }

        public decimal? dimx { get; set; }

        public decimal? dimy { get; set; }

        public decimal? dimz { get; set; }

        public int color { get; set; }
    }
}
