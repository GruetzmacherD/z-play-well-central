﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.Import
{
    public class Set_Inv
    {
        public string set_id { get; set; }

        public string part_id { get; set; }

        public string color_id { get; set; }

        public bool alternate { get; set; }

        public int quantity { get; set; }

        public bool counter_part { get; set; }

        public bool extra { get; set; }

        public int match_id { get; set; }
    }
}
