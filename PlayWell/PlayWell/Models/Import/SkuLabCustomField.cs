﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    [JsonObject ("custom_fields")]
    public class SkuLabCustomField
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("field")]
        public string Type { get; set; }

        [JsonProperty ("value")]
        public string BrickLinkId { get; set; }
    }
}
