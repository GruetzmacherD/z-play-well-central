﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class SkuLabItem
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty ("name")]
        public string Name { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("cost")]
        public decimal? Cost { get; set; }

        public decimal? Retail { get; set; }

        [JsonProperty ("custom_fields")]
        public ICollection<SkuLabCustomField> CustomFields { get;  set; }

        [JsonProperty("weight")]
        public double? Weight { get; set; }

        [JsonProperty("weight_unit")]
        public string WeightUnit { get; set; }

        [JsonProperty("length")]
        public int? Length { get; set; }

        [JsonProperty("width")]
        public int? Width { get; set; }

        [JsonProperty("height")]
        public int? Height { get; set; }
    }
}
