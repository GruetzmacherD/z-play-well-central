﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class SkuLabWarehouse
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty ("type")]
        public string Type { get; set; }

        //[JsonProperty ("address")]
        //public string Address { get; set; }
    }
}
