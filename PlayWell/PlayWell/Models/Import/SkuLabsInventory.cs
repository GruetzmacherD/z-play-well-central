﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    [JsonObject ("inventory")]
    public class SkuLabsInventory
    {
        public string Id { get; set; }

        [JsonProperty ("location_id")]
        public string LocationId { get; set; }

        [JsonProperty ("item_id")]
        public string SkuLabsGeneratedItemId { get; set; }

        [JsonProperty ("on_hand")]
        public int? QuantityOnHand { get; set; }


    }
}
