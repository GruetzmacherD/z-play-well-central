﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.Import
{
    public class Sold
    {
        public string item_id { get; set; }

        public string type { get; set; }

        public DateTime date { get; set; }

        public string cond { get; set; }

        public int quantity { get; set; }

        public decimal price { get; set; }

        public string buyer_country { get; set; }

        public string seller_country { get; set; }
    }
}
