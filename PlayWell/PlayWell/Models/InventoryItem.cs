﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class InventoryItem
    {
        public int Id { get; set; }

        public int QuantityTotal { get; set; }

        public int LegoItemId { get; set; }

        public LegoItem LegoItem { get; set; }

        public int ConditionId { get; set; }

        public Condition Condition { get; set; }

        public string BrickLinkLotId { get; set; }

        public string Sku { get; set; }

        public decimal? Cost { get; set; }

        public ICollection<InventoryLocation> InventoryLocations { get; set; }
    }
}
