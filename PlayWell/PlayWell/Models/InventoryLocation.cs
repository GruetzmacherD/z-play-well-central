﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class InventoryLocation
    {
        public int Id { get; set; }

        public int LocationId { get; set; }

        public Location Location { get; set; }

        public int InventoryItemId { get; set; }

        public InventoryItem InventoryItem { get; set; }

        public int Quantity { get; set; }
    }
}
