﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Playwell;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PlayWell.Models.InventoryViewModels
{
    public class InventoryViewModel
    {
        public string Filter { get; set; }

        public int Type { get; set; }

        public string View { get; set; }

        public int AmountToDisplay { get; set; }

        public LegoList ChosenList { get; set; }

        public List<LegoItem> LegoItems { get; set; }

        public IEnumerable<LegoColor> LegoColors { get; set; }

        public List<LegoList> LegoLists { get; set; }

        public IEnumerable<LegoType> LegoTypes { get; set; }

        public IEnumerable<PricingMethod> PricingMethods { get; set; }

        public decimal? MSRP { get; set; }

        public int Counter { get; set; }

        public string Previous { get; set; }

        public int PageNumber { get; set; }

        public int TotalResults { get; set; }

        public double TotalPages { get; set; }
    }
}
