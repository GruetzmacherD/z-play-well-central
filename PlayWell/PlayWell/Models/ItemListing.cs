﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class ItemListing
    {
        public int Id { get; set; }

        public int InventoryItemId { get; set; }

        public InventoryItem InventoryItem { get; set; }

        public int SalesChannelId { get; set; }

        public SalesChannel SalesChannel { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string ListingSku { get; set; }
    }
}
