﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class LegoItem
    {
        public int Id { get; set; }

        public int LegoTypeId { get; set; }

        public virtual LegoType LegoType { get; set; }

        public string BrickLinkId { get; set; }

        public int LegoColorId { get; set; }

        public virtual LegoColor LegoColor { get; set; }

        public string Description { get; set; }

        public decimal? MSRP { get; set; }

        public int LegoCategoryId { get; set; }

        public virtual LegoCategory LegoCategory { get; set; }

        public int? Year { get; set; }

        public double? Weight { get; set; }

        public double? DimensionX { get; set; }

        public double? DimensionY { get; set; }

        public double? DimensionZ { get; set; }

        public DateTime? LastUpdated { get; set; }

        public int PricingMethodId { get; set; }

        public virtual PricingMethod PricingMethod { get; set; }

        public decimal Price { get; set; }

        public int? NumberOfSaleTransactions { get; set; }

        public int? SoldQuantityTotal { get; set; }

        public decimal? SoldAveragePrice { get; set; }

        public int? NumberOfForSaleLots { get; set; }

        public int? ForSaleQuantityTotal { get; set; }

        public decimal? ForSaleAveragePrice { get; set; }

        public DateTime? DateFirstSold { get; set; }

        public Func<int, bool> CheckFavorite;

        public Func<int, decimal> CheckPrice;

        public bool IsFavorite
        {
            get
            {
                bool result = false;

                if (this.CheckFavorite != null)
                {
                    result = this.CheckFavorite(this.Id);
                }

                return result;
            }
        }

        public decimal SuggestedPrice
        {
            get
            {
                decimal result = 0.00m;

                if (this.CheckPrice != null)
                {
                    result = this.CheckPrice(this.Id);
                }

                return result;
            }
        }

        public ICollection<InventoryItem> InventoryItems { get; set; }

        public ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }

        public ICollection<ForSaleItem> ForSaleItems { get; set; }

        public ICollection<SoldItem> SoldItems { get; set; }

        [InverseProperty("Set")]
        public ICollection<SetBridge> Sets { get; set; }

        [InverseProperty("Part")]
        public ICollection<SetBridge> SetParts { get; set; }
    }
}
