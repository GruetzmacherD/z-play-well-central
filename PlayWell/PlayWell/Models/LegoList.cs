﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class LegoList
    {
        public int Id { get; set; }

        public string ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public DateTime LastModified { get; set; }

        public string Name { get; set; }

        public virtual ICollection<ListBridge> ListBridges { get; set; }
    }
}
