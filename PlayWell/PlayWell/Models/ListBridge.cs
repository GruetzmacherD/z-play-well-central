﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class ListBridge
    {
        public int Id { get; set; }

        public int LegoItemId { get; set; }

        public virtual LegoItem LegoItem { get; set; }

        public int LegoListId { get; set; }

        public virtual LegoList LegoList { get; set; }
    }
}
