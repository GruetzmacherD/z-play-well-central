﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.ListViewModels
{
    public class ListViewModel
    {
        public List<LegoList> LegoLists { get; set; }
    }
}
