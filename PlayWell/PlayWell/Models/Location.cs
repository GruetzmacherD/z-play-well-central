﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    
    public class Location
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }

        public ICollection<InventoryLocation> InventoryLocations { get; set; }

        public ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }

        public ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
