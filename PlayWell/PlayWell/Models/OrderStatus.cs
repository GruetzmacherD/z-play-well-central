﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class OrderStatus
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public ICollection<PurchaseOrder> PurchaseOrders { get; set; }

        public ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
    }
}
