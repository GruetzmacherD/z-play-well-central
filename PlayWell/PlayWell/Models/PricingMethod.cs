﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class PricingMethod
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<LegoItem> LegoItems { get; set; }
    }
}
