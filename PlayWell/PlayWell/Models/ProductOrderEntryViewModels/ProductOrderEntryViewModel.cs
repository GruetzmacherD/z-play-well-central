﻿using PlayWell.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models.ProductOrderEntryViewModels
{
    public class ProductOrderEntryViewModel
    {

        public List<Condition> Conditions { get; set; }

        public Condition Condition { get; set; }

        public string SearchString { get; set; }

        public List<ApplicationUser> Users { get; set; }

        public ApplicationUser User { get; set; }

        public List<LegoItem> SearchResult { get; set; }

        public LegoItem ItemToAdd { get; set; }

        public List<Warehouse> Warehouses { get; set; }

        public int WarehouseId { get; set; }
    }
}
