﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class PurchaseOrder
    {
        public int Id { get; set; }

        public int StoreId { get; set; }

        public Vendor Vendor { get; set; }

        public string StoreLocation { get; set; }

        public int ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public int OrderStatusId { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public decimal CommissionTotal { get; set; }

        public decimal ShippingTotal { get; set; }

        public decimal TaxTotal { get; set; }

        public decimal PriceTotal { get; set; }
    }
}
