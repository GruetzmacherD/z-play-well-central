﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class PurchaseOrderLine
    {
        public int Id { get; set; }

        public int ConditionId { get; set; }

        public Condition Condition { get; set; }

        public int LegoItemId { get; set; }

        public LegoItem LegoItem { get; set; }

        public int PurchaseOrderId { get; set; }

        public PurchaseOrder PurchaseOrder { get; set; }

        public int OrderStatusId { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public int LocationId { get; set; }

        public Location Location { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal Shipping { get; set; }

        public decimal Tax { get; set; }

        public decimal Commission { get; set; }
    }
}
