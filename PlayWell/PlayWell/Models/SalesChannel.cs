﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class SalesChannel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
