﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class SetBridge
    {
        public int Id { get; set; }

        public int SetId { get; set; }

        [ForeignKey("SetId")]
        public LegoItem Set { get; set; }

        public int ItemId { get; set; }

        [ForeignKey("ItemId")]
        public LegoItem Part { get; set; }

        public int Quantity { get; set; }

        public int MatchId { get; set; }

        public bool? Alternate { get; set; }

        public bool? Extra { get; set; }

        public bool? CounterPart { get; set; }
    }
}
