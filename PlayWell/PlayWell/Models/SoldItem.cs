﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class SoldItem
    {
        public int Id { get; set; }

        public int LegoItemId { get; set; }

        public LegoItem LegoItem { get; set; }

        public int ConditionId { get; set; }

        public Condition Condition { get; set; }

        public DateTime SaleDate { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public int BuyerCountryId { get; set; }

        [ForeignKey("BuyerCountryId")]
        public Country BuyerCountry { get; set; }

        public int SellerCountryId { get; set; }

        [ForeignKey("SellerCountryId")]
        public Country SellerCountry { get; set; }
    }
}
