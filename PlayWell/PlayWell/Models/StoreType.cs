﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class VendorType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Vendor> Vendors { get; set; }
    }
}
