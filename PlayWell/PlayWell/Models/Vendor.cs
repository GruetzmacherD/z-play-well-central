﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class Vendor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int StoreTypeId { get; set; }

        public VendorType StoreType { get; set; }

        public int Quantity { get; set; }
    }
}
