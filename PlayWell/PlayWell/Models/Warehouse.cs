﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayWell.Models
{
    public class Warehouse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Location> Locations { get; set; }

        public ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
