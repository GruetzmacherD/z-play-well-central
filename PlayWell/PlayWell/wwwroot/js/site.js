﻿// When the document ready for javascript to run
$(document).ready(function () {
    // $('#chosenList').val('@Model.ChosenList.Id');

    // Compiles totals
    $.get("http://playwellusa.com/Inventory/InitiateTotals").done(function (data1) {

        if (data1["isFinalPage"] === "true") {
            $(".nextButton").removeClass('btn btn-default').addClass("btn btn-default disabled");
        }
        // Sets total results in dom
        $.get("http://playwellusa.com/Inventory/GetTotalResults").done(function (data2) {

            var element = document.getElementById("TotalResults");
            element.innerHTML = data2;
        });
        // Sets total pages in dom
        $.get("http://playwellusa.com/Inventory/GetTotalPages").done(function (data3) {

            var element = document.getElementById("TotalPages");
            element.innerHTML = data3;
        });
    });
});

// When the view changes submit form
$("#chosenView").change(function () {
    $("#searchForm").submit();
});

// When the amount to display changes submit form
$("#displayAmount").change(function () {
    $("#searchForm").submit();
});

// Submit form on enter key
$("#filter").on("keypress", function (e) {
    if (e.which === 13 || e.which === 9) {
        $("#searchForm").submit();
    }
});

// Don't sumbmit favorites forms
$(".favoriteForm").submit(function (e) {
    e.preventDefault();
});

// When a favorite star is clicked
$(".favoriteStar").click(function () {
    var itemDetails = new Object();
    // Get the item id of the lego item in the dom
    var itemId = $(this).attr('id');
    itemDetails.itemId = itemId.substring(11, itemId.length);

    // Toggle the star to filled in or empty
    if ($(this).attr('class') === 'favoriteStar glyphicon glyphicon-star-empty bigger-star'){
        $(this).removeClass('favoriteStar glyphicon glyphicon-star-empty bigger-star').addClass("favoriteStar glyphicon glyphicon-star bigger-star");
    } else {
        $(this).removeClass('favoriteStar glyphicon glyphicon-star bigger-star').addClass("favoriteStar glyphicon glyphicon-star-empty bigger-star");
    }

    $.get("http://playwellusa.com/Inventory/AddToList", itemDetails).done(function (data) {

    });
});

// When the pricing method drop-down is changed
$(".pricingMethod").change(function () {
    var itemDetails = new Object();
    // Get the item id from the dom
    var itemId = $(this).attr('id');
    // Create object to send in ajax call
    itemDetails.id = itemId.substring(14, itemId.length);
    itemDetails.pricingMethodId = $(this).val();

    $('SuggestedPrice_' + itemDetails.id).html('test');

    // Send item id and pricing method to api in object to update price
    $.get("http://playwellusa.com/api/LegoItem/UpdatePricingMethod", itemDetails).done(function (data) {

        var elementId = 'SuggestedPrice_' + itemDetails.id;
        var element = document.getElementById(elementId);
        element.innerHTML = parseFloat(data).toFixed(2);
    });
});

// When the chosen list changes send the id
$("#chosenList").change(function () {
    var itemDetails = new Object();
    var listId = $("#chosenList").val();
    itemDetails.listId = listId.toString();

    $.get("http://playwellusa.com/Inventory/LoadChosenList", itemDetails).done(function (data) {

        $("#searchForm").submit();
    });
});

// When the price field changes
$(".priceChange").change(function () {
    var itemDetails = new Object();
    // Get the item id from the dom
    var itemId = $(this).attr('id');
    // Create object to send in ajax call
    itemDetails.id = itemId.substring(6, itemId.length);
    itemDetails.price = $(this).val();

    $.get("http://playwellusa.com/Inventory/SetPrice", itemDetails).done(function (data) {
    });
});